from pdf2image import convert_from_path

poppler_path = "C:\\Users\\sam-dev\\Desktop\\Release-21.03.0\\poppler-21.03.0\\Library\\bin"



def get_png():

 
 
    # Store Pdf with convert_from_path function
    images = convert_from_path('data\onepage.pdf', poppler_path= poppler_path)
    
    for i in range(len(images)):
    
        # Save pages as images in the pdf
        images[i].save('data\page'+ str(i) +'.png', 'PNG')


