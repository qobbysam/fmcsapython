
import os
import sys
#from pdfrw import PdfReader
import pdfrw
from pprint import pprint

from pdfforms import inspect

# -*- coding: utf-8 -*-

from collections import OrderedDict
from PyPDF2 import PdfFileWriter, PdfFileReader


BASE_DIR = os.getcwd()

IN_PDF_LOCATION =  os.path.join(BASE_DIR, 'data','knownpdf' ,'')


pdf_file_big = IN_PDF_LOCATION +  'ohio_ifta_one.pdf'



# def _getFields(obj, tree=None, retval=None, fileobj=None):
#     """
#     Extracts field data if this PDF contains interactive form fields.
#     The *tree* and *retval* parameters are for recursive use.

#     :param fileobj: A file object (usually a text file) to write
#         a report to on all interactive form fields found.
#     :return: A dictionary where each key is a field name, and each
#         value is a :class:`Field<PyPDF2.generic.Field>` object. By
#         default, the mapping name is used for keys.
#     :rtype: dict, or ``None`` if form data could not be located.
#     """
#     fieldAttributes = {'/FT': 'Field Type', '/Parent': 'Parent', '/T': 'Field Name', '/TU': 'Alternate Field Name',
#                        '/TM': 'Mapping Name', '/Ff': 'Field Flags', '/V': 'Value', '/DV': 'Default Value'}
#     if retval is None:
#         retval = OrderedDict()
#         catalog = obj.trailer["/Root"]
#         # get the AcroForm tree
#         if "/AcroForm" in catalog:
#             tree = catalog["/AcroForm"]
#         else:
#             return None
#     if tree is None:
#         return retval

#     obj._checkKids(tree, retval, fileobj)
#     for attr in fieldAttributes:
#         if attr in tree:
#             # Tree is a field
#             obj._buildField(tree, retval, fileobj, fieldAttributes)
#             break

#     if "/Fields" in tree:
#         fields = tree["/Fields"]
#         for f in fields:
#             field = f.getObject()
#             obj._buildField(field, retval, fileobj, fieldAttributes)

#     return retval


# def get_form_fields(infile):
#     infile = PdfFileReader(open(infile, 'rb'))
#     fields = _getFields(infile)
#     return OrderedDict((k, v.get('/V', '')) for k, v in fields.items())

ANNOT_KEY = '/Annots'
ANNOT_FIELD_KEY = '/T'
ANNOT_VAL_KEY = '/V'
ANNOT_RECT_KEY = '/Rect'
SUBTYPE_KEY = '/Subtype'
WIDGET_SUBTYPE_KEY = '/Widget'


template_pdf = pdfrw.PdfReader(pdf_file_big)



def print_fields():

    # pdf_file_name = pdf_file_big

    # pprint(get_form_fields(pdf_file_name))

    for page in template_pdf.pages:
        annotations = page[ANNOT_KEY]
        for annotation in annotations:
            if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY:
                if annotation[ANNOT_FIELD_KEY]:
                    key = annotation[ANNOT_FIELD_KEY][1:-1]
                    print(key)




