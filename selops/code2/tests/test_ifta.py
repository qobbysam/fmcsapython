"""
This module contains web test cases for the tutorial.
Tests use Selenium WebDriver with Chrome and ChromeDriver.
The fixtures set up and clean up the ChromeDriver instance.
"""
import sys
sys.path.append('../')
import pytest

import time

#from pages.loginPage import DuckDuckGoResultPage
from pages.getHomeBeforeLogin import HomeLogin, HomeAfterLogin, Ifta_steps
from pages.continue_ifta import Recover_Started

from  data_in.data_producer import homeDataProducer, homeDataProducer_not_fixture


#import conftest


def ifta_process():
  browser = conftest.browser_not_fixture("firefox", 10)

  big_data = homeDataProducer_not_fixture()

  start_page = HomeLogin(browser, big_data['homedata'])

  start_page.load()

  time.sleep(5)






# def test_basic_duckduckgo_search(browser,homeDataProducer):
#   # Set up test case data
#   PHRASE = 'panda'

#   # Search for the phrase
#   search_page = DuckDuckGoSearchPage(browser, homeDataProducer)
#   search_page.load()
#   search_page.search(PHRASE)

#   # Verify that results appear
#   result_page = DuckDuckGoResultPage(browser)
#   assert result_page.link_div_count() > 0
#   assert result_page.phrase_result_count(PHRASE) > 0
#   assert result_page.search_input_value() == PHRASE

def test_gateway_login(browser, homeDataProducer):
    big_data = homeDataProducer

    start_page = HomeLogin(browser, big_data['homedata'])

    start_page.load()

    time.sleep(5)

    start_page.close_pop_one_up(**big_data)

    time.sleep(1)

    start_page.close_pop_two_up(**big_data)

    time.sleep(5)

    start_page.loginformfill(**big_data)


    time.sleep(10)

    page_1 = Recover_Started(browser, big_data)

    page_1.recover_started(**big_data)

    page_1.begin_step_two(**big_data)

    # time.sleep(3)



# def test_ifta(browser, homeDataProducer):

#   # big_data = homeDataProducer

#   # start_page = HomeLogin(browser, big_data['homedata'])

#   # start_page.load()

#   # time.sleep(5)

#   # start_page.close_pop_one_up(**big_data)

#   # time.sleep(4)

#   # start_page.close_pop_two_up(**big_data)

#   # time.sleep(5)

#   # start_page.loginformfill(**big_data)


#   time.sleep(5)

#   next_page_1 = HomeAfterLogin(browser, **big_data)

#   next_page_1.click_ifta_home(**big_data)

#   #next_page_1.pre_start_ifta(**big_data)


#   next_page_2 = Ifta_steps(browser,**big_data)

#   next_page_2.step_one(**big_data)

#   next_page_2.step_two(**big_data)

# def test_recover(browser, homeDataProducer):

#   big_data = homeDataProducer







