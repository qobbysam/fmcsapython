  
"""
This module contains DuckDuckGoSearchPage,
the page object for the DuckDuckGo search page.
"""

import sys
sys.path.append('../')

import time

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from utils.low_level import LowLevelWriter


class HomeLogin:
  
  #URL = 'https://www.duckduckgo.com'

  

  #SEARCH_INPUT = (By.ID, 'search_form_input_homepage')



  def __init__(self, browser, iterable=(), **data):
  	self.__dict__.update(iterable, **data)
  	self.browser = browser
  	self.local_writer = LowLevelWriter(browser)

  def load(self):
    self.browser.get(self.URL)

  # def search(self, phrase):
  #   search_input = self.browser.find_element(*self.SEARCH_INPUT)
  #   search_input.send_keys(phrase + Keys.RETURN)


  def close_pop_one_up(self, **data):
  	data_ = data
  	#local_writer = LowLevelWriter(self.browser)
  	close_pop_up_path = data_['homedata']['selectors']['close_popup_one_path']

  	self.local_writer.button_clicker(close_pop_up_path)

  	print("popup closed")

  def close_pop_two_up(self, **data):
  	data_ = data
  	#local_writer = LowLevelWriter(self.browser)
  	close_pop_up_path = data_['homedata']['selectors']['close_popup_two_path']

  	self.local_writer.button_clicker(close_pop_up_path)

  	print("popup closed")



  def loginformfill(self, **data):

  	#local_writer = LowLevelWriter(self.browser)

  	data_ = data
  	username = data_['homedata']['loginInfo']['username']
  	username_path = data_['homedata']['selectors']['username_path']

  	password = data_['homedata']['loginInfo']['password']
  	password_path = data_['homedata']['selectors']['password_path']

  	submit_path = data_['homedata']['selectors']['submit_path']

  	

  	self.local_writer.button_clicker(username_path)
  	self.local_writer.text_input_writer(username_path, username)
  	#time.sleep(3)
  	self.local_writer.button_clicker(password_path)
  	self.local_writer.text_input_writer(password_path, password)
  	#time.sleep(3)
  	self.local_writer.button_clicker(submit_path)


  	print("login form filled with this data %s " % (data_))



class HomeAfterLogin:

  def __init__(self, browser, iterable=(), **data):
    self.__dict__.update(iterable, **data)
    self.browser = browser
    self.local_writer = LowLevelWriter(browser)



  def click_ifta_home(self, **data):

    time.sleep(3)

    data_ = data['homeAfterLogin']

    selectors = data_['selectors']


    text_to_be = 'International Fuel Tax Return'
    val_at_path_one = self.local_writer.get_value_at_path(selectors['click_ifta_one_path'])

    val_at_path_two = self.local_writer.get_value_at_path(selectors['click_ifta_two_path'])

    if val_at_path_one is not None:

      self.local_writer.button_clicker(selectors['click_ifta_one_path'])

    else:
      self.local_writer.button_clicker(selectors['click_ifta_two_path'])



  def pre_start_ifta(self, **data):

    time.sleep(3)

    data_ = data['homeAfterLogin']
    selectors = data_['selectors']


    self.local_writer.checkbox_checker(selectors['click_agree_path'])

    self.local_writer.button_clicker(selectors['click_next_path'])





class Ifta_steps:

  def __init__(self, browser, iterable=(), **data):
    self.__dict__.update(iterable, **data)
    self.browser = browser
    self.local_writer = LowLevelWriter(browser)



  def step_one(self, **data):
    data_ = data['ifta_step_one']

    selectors = data_['selectors']

    values = data_['step_one_values']


    self.local_writer.text_input_writer(selectors['year_path'], values['year'])

    self.local_writer.dropdown_selector(selectors['period_path'], values['period'])

    self.local_writer.dropdown_selector(selectors['return_type_path'], values['return_type'])


    self.local_writer.button_clicker(selectors['click_next_path'])



  def step_two(self, **data):

    data_ = data['ifta_step_two']


    selectors = data_['selectors']

    values = data_['step_two_values']


    ifta_data = data['miles_gallons_data']


    jurisdication_mile_gallon_path_list = [selectors['jurisdiction_select_path'], selectors['mile_path'], selectors['gallons_path']]

    add_btn_path = selectors['add_btn_path']






    self.local_writer.text_input_writer(selectors['num_of_vehicles_path'], values['num_of_vehicles'])


    self.local_writer.button_clicker(selectors['show_shedule_path'])

    time.sleep(3)
    for item in ifta_data:

      ifta_value = ifta_data[item]

      ifta_value_list = [ifta_value['state'], ifta_value['miles'], ifta_value['gallons']]


      self.local_writer.miles_gallons_ifta_doer(jurisdication_mile_gallon_path_list, ifta_value_list, add_btn_path)

      time.sleep(2)

    self.local_writer.button_clicker(selectors['return_to_summary_path'])

    self.local_writer.button_clicker('save_btn_path')


