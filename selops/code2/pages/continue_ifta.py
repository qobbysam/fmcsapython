import sys
sys.path.append('../')

import time

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from utils.low_level import LowLevelWriter

class HomeAfterLogin:

  def __init__(self, browser, iterable=(), **data):
    self.__dict__.update(iterable, **data)
    self.browser = browser
    self.local_writer = LowLevelWriter(browser)



  def click_ifta_home(self, **data):

    time.sleep(3)

    data_ = data['homeAfterLogin']

    selectors = data_['selectors']


    text_to_be = 'International Fuel Tax Return'
    val_at_path_one = self.local_writer.get_value_at_path(selectors['click_ifta_one_path'])

    val_at_path_two = self.local_writer.get_value_at_path(selectors['click_ifta_two_path'])

    if val_at_path_one is not None:

      self.local_writer.button_clicker(selectors['click_ifta_one_path'])

    else:
      self.local_writer.button_clicker(selectors['click_ifta_two_path'])



  def pre_start_ifta(self, **data):

    time.sleep(3)

    data_ = data['homeAfterLogin']
    selectors = data_['selectors']


    self.local_writer.checkbox_checker(selectors['click_agree_path'])

    self.local_writer.button_clicker(selectors['click_next_path'])



class Recover_Started:

    def __init__(self, browser, iterable=(), **data):
        self.__dict__.update(iterable, **data)
        self.browser = browser
        self.local_writer = LowLevelWriter(browser)

    def recover_started(self, **data):

        data_ = data['recover_data']

        selectors=data_['selectors']

        time.sleep(3)
        self.local_writer.button_clicker(selectors['in_progress_path'])

        time.sleep(3)

        self.local_writer.button_clicker(selectors['ifta_click_path'])


    def begin_step_two(self, **data):

        data_ = data['recover_data']

        selectors = data_['selectors']

        jurisdication_mile_gallon_path_list = [selectors['jurisdiction_select_path'], selectors['mile_path'], selectors['gallons_path']]
        
        add_btn_path = selectors['add_btn_path']

        ifta_data = data['miles_gallons_data']

        
        #self.local_writer.text_input_writer(selectors['num_of_vehicles_path'], values['num_of_vehicles'])

        #self.local_writer.text_input_writer(selectors['show_schedule_path'], "\n")
        #self.local_writer.button_clicker(selectors['show_shedule_path'])
        time.sleep(10)
        self.local_writer.click_btn_js(selectors['show_schedule_path'])
        time.sleep(5)
        for item in ifta_data:

            ifta_value = ifta_data[item]

            ifta_value_list = [ifta_value['state'], ifta_value['miles'], ifta_value['gallons']]


            self.local_writer.miles_gallons_ifta_doer(jurisdication_mile_gallon_path_list, ifta_value_list, add_btn_path)

            time.sleep(6)

        self.local_writer.button_clicker(selectors['return_to_summary_path'])
        time.sleep(10)
        self.local_writer.click_btn_js('save_work_path')
        
        


    
    def return_to_summary(self, **data):

        data_ = data['recover_data']

        selectors = data_['selectors']

        self.local_writer.button_clicker(selectors['return_to_summary_path'])

        time.sleep(3)

        self.local_writer.button_clicker(selectors['save_work_path'])











        





# class Ifta_steps:

#   def __init__(self, browser, iterable=(), **data):
#     self.__dict__.update(iterable, **data)
#     self.browser = browser
#     self.local_writer = LowLevelWriter(browser)



#   def step_one(self, **data):
#     data_ = data['ifta_step_one']

#     selectors = data_['selectors']

#     values = data_['step_one_values']


#     self.local_writer.text_input_writer(selectors['year_path'], values['year'])

#     self.local_writer.dropdown_selector(selectors['period_path'], values['period'])

#     self.local_writer.dropdown_selector(selectors['return_type_path'], values['return_type'])


#     self.local_writer.button_clicker(selectors['click_next_path'])



#   def step_two(self, **data):

#     data_ = data['ifta_step_two']


#     selectors = data_['selectors']

#     values = data_['step_two_values']


#     ifta_data = data['miles_gallons_data']


#     jurisdication_mile_gallon_path_list = [selectors['jurisdiction_select_path'], selectors['mile_path'], selectors['gallons_path']]

#     add_btn_path = selectors['add_btn_path']






