
import os

from tinydb import TinyDB, Query
from tinydb.operations import add

import csv


BASE_DIR = os.getcwd()

CSV_FILENAME = "ahsq3.csv"
                                                                                                             
CSV_IN_FILE = 	os.path.join(BASE_DIR,'data_in','csv', 'fuel_in', CSV_FILENAME)

TN_NAME = "halganq2fuel2021"

TINY_FILENAME_ALL = TN_NAME + ".json"

TINY_FILENAME_TOTALS = TN_NAME + "_totals.json"

TINY_FILE_ALL = os.path.join(BASE_DIR, 'data_in', 'tiny', TINY_FILENAME_ALL)

TINY_FILE_TOTAL = os.path.join(BASE_DIR,'data_in', 'tiny', TINY_FILENAME_TOTALS)

data_out = []


KNOWN_STATES = []

DB = TinyDB(TINY_FILE_ALL)

DB_FINAL = TinyDB(TINY_FILE_TOTAL)

GALLONS = Query()



State_total = []

DB.truncate()

def write_known_state(state, value):

	#DB.insert({'state': state, 'gallons': value})
	#DB.upsert({'state': state, value})

	gals = []

	list_ = DB.get((GALLONS.state == state))

	gals = list_['gallons']


	gals.append(value)

	list_['gallons'] = gals

	new_list = gals

	#DB.write_back(list_)

	DB.update(list_, GALLONS.state == state)

	#DB.update(list_, GALLONS.state.exists())
	# for item in DB.search(GALLONS.state == state):

	# 	gals = item['gallons']

	# 	gals.append(value)


		#item['gallons'] = 

	#DB.update(set(state, gals), )

	#DB.update({'state': state, 'gallons': gals})

	#DB.update(add('gallons', value), GALLONS.state== state)



def write_new_state(state,value):

	count = len(KNOWN_STATES) + 1

	values_list = [value]

	DB.insert({'state': state, 'gallons': [value], 'count': count  })
	
	KNOWN_STATES.append(state)


def list_creator(state, value):

	if state in KNOWN_STATES:

		write_known_state(state, value)

	else:
		write_new_state(state,value)


def state_value(text_in):

	out = text_in.split('+')

	state = out[0]

	out_val = out[1:]

	out_val = list(map(int, out_val))

	total = sum(out_val)


	out_list = [state, total]

	return out_list



def starter_cmd():

	i = 55

	while i < 56:
		input_text = enter_values()

		if input_text == "endme":

			finish_process()

			i = 57



		else:


			#state_value = input_text.split('+')

			state_value_ = state_value(input_text)

			state, value = state_value_

			list_creator(state.upper(),value)

def starter_csv():


	with open(CSV_IN_FILE,  newline='') as csvfile:
		reader = csv.DictReader(csvfile)

		for row in reader:

			state = row['STATE']
			gallons = row['GALLONS']


			if state == "" or state is None:
				print("skipped state   %s" %(state))

			else:


				list_creator(state.upper(), str(gallons))

		finish_process()
		print("starter csv complete")


def enter_values():

	input_text = input()

	return input_text
	

def write_finals(state, totals):

	DB_FINAL.insert({'state_final': state, 'gallons_total': totals})

def print_state_totals():

	for state in KNOWN_STATES:
		m = DB.search(GALLONS.state == state)

		gallons = m[0]['gallons']

		int_gallons = [int(float(i)) for i in gallons]
		sum_of_gallons = sum(int_gallons)
		state = state

		write_finals(state, sum_of_gallons)


	






		print (m)

def finish_process():



	all_here = DB.all()
	
	print(all_here)


	print(KNOWN_STATES)

	print(len(KNOWN_STATES))


	print_state_totals()



def main():

	#starter_csv()
	starter_cmd()

	#starter()