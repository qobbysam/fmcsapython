import json
import pytest

import os

import sys
sys.path.append('../')

BASE_DIR = os.path.abspath(os.getcwd())

DATA_PATH = os.path.join(BASE_DIR ,'data_in','execs','hog.json')




@pytest.fixture(scope='session')
def homeDataProducer():
  # Read the JSON config file and returns it as a parsed dict
  with open(DATA_PATH) as config_file:
    data = json.load(config_file)
  return data


def homeDataProducer_not_fixture():
  # Read the JSON config file and returns it as a parsed dict
  with open(DATA_PATH) as config_file:
    data = json.load(config_file)
  return data