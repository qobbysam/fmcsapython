"""
This module contains web test cases for the tutorial.
Tests use Selenium WebDriver with Chrome and ChromeDriver.
The fixtures set up and clean up the ChromeDriver instance.
"""
import sys
sys.path.append('../')
import pytest

import time

#from pages.loginPage import DuckDuckGoResultPage
from pages.getHomeBeforeLogin import HomeLogin, HomeAfterLogin, Ifta_steps

from  data_in.data_producer import homeDataProducer, homeDataProducer_not_fixture


from tests import conftest

def ifta_process():
  browser = conftest.browser_not_fixture("firefox", 10)

  big_data = homeDataProducer_not_fixture()

  start_page = HomeLogin(browser, big_data['homedata'])

  start_page.load()

  time.sleep(5)

  start_page.close_pop_one_up(**big_data)

  time.sleep(4)

  start_page.close_pop_two_up(**big_data)






# def test_basic_duckduckgo_search(browser,homeDataProducer):
#   # Set up test case data
#   PHRASE = 'panda'

#   # Search for the phrase
#   search_page = DuckDuckGoSearchPage(browser, homeDataProducer)
#   search_page.load()
#   search_page.search(PHRASE)

#   # Verify that results appear
#   result_page = DuckDuckGoResultPage(browser)
#   assert result_page.link_div_count() > 0
#   assert result_page.phrase_result_count(PHRASE) > 0
#   assert result_page.search_input_value() == PHRASE





