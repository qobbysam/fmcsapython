
import os
import csv
import time
from tinydb import TinyDB, Query
from tinydb.operations import add


BASE_DIR = os.getcwd()

TN_NAME = "hogaq22021"

TINY_FILENAME_ALL = TN_NAME + ".json"

TINY_FILENAME_TOTALS = TN_NAME + "_totals.json"

TINY_FILENAME_FINAL = TN_NAME + "_final.json"

TINY_FILE_ALL = os.path.join(BASE_DIR, 'data_in', 'tiny', TINY_FILENAME_FINAL)

TINY_FILE_TOTAL = os.path.join(BASE_DIR,'data_in', 'tiny', TINY_FILENAME_TOTALS)

CSV_FILENAME = 'hog.csv'

CSV_IN_FILE = 	os.path.join(BASE_DIR,'data_in','csv', 'miles_in', CSV_FILENAME)


data_out = []


KNOWN_STATES = []

DB = TinyDB(TINY_FILE_ALL)
#DB_MILES = TinyDB('total_miles')
DB_GALLONS = TinyDB(TINY_FILE_TOTAL)

MILE_GALLONS = Query()



State_total = []

#DB.truncate()


us_state_abbrev = {
    'ALABAMA': 'AL',
    'ALASKA': 'AK',
    'AMERICAN SAMOA': 'AS',
    'ARIZONA': 'AZ',
    'ARKANSAS': 'AR',
    'CALIFORNIA': 'CA',
    'COLORADO': 'CO',
    'CONNECTICUT': 'CT',
    'DELAWARE': 'DE',
    'DISTRICT OF COLUMBIA': 'DC',
    'FLORIDA': 'FL',
    'GEORGIA': 'GA',
    'GUAM': 'GU',
    'HAWAII': 'HI',
    'IDAHO': 'ID',
    'ILLINOIS': 'IL',
    'INDIANA': 'IN',
    'IOWA': 'IA',
    'KANSAS': 'KS',
    'KENTUCKY': 'KY',
    'LOUISIANA': 'LA',
    'MAINE': 'ME',
    'MARYLAND': 'MD',
    'MASSACHUSETTS': 'MA',
    'MICHIGAN': 'MI',
    'MINNESOTA': 'MN',
    'MISSISSIPPI': 'MS',
    'MISSOURI': 'MO',
    'MONTANA': 'MT',
    'NEBRASKA': 'NE',
    'NEVADA': 'NV',
    'NEW HAMPSHIRE': 'NH',
    'NEW JERSEY': 'NJ',
    'NEW MEXICO': 'NM',
    'NEW YORK': 'NY',
    'NORTH CAROLINA': 'NC',
    'NORTH DAKOTA': 'ND',
    'NORTHERN MARIANA ISLANDS':'MP',
    'OHIO': 'OH',
    'OKLAHOMA': 'OK',
    'OREGON': 'OR',
    'PENNSYLVANIA': 'PA',
    'PUERTO RICO': 'PR',
    'RHODE ISLAND': 'RI',
    'SOUTH CAROLINA': 'SC',
    'SOUTH DAKOTA': 'SD',
    'TENNESSEE': 'TN',
    'TEXAS': 'TX',
    'UTAH': 'UT',
    'VERMONT': 'VT',
    'VIRGIN ISLANDS': 'VI',
    'VIRGINIA': 'VA',
    'WASHINGTON': 'WA',
    'WASHINGTON DC': 'DC',
    'WEST VIRGINIA': 'WV',
    'WISCONSIN': 'WI',
    'WYOMING': 'WY'
}


abbrev_us_state = dict(map(reversed, us_state_abbrev.items()))



#clean state

def cleanstate(instate):

	upper_instate = instate.upper()

	out_state = upper_instate

	for key in us_state_abbrev:


		if upper_instate == key:

			out_state = us_state_abbrev[key]

		# elif (abbrev_us_state[upper_instate] != ) == key:

		# 	out_state = upper_instate

		else:
			out_state = us_state_abbrev[upper_instate]


	print(out_state)

	return out_state


def write_state_gallons(data_in):

	print(data_in)

	data_ = data_in

	state,miles,gallons = data_


	DB.insert({'state': state, 'miles': miles, 'gallons': gallons  })
	
	#KNOWN_STATES.append(state)


#get csv with miles data

def return_data_csv():

	with open(CSV_IN_FILE,  newline='') as csvfile:
		reader = csv.DictReader(csvfile)

		for row in reader:

			state = row['STATE']
			miles = row['MILES']

			gallons = '0'

			clean_state = cleanstate(state)

			list_state_miles_gallons = [clean_state,miles,gallons]

			write_state_gallons(list_state_miles_gallons)








def update_miles_gallon(data_in):

	miles = DB.all()
	# gals = []

	# #list_ = DB.get((GALLONS.state == state))

	# gals = list_['gallons']




	# gals.append(value)

	# list_['gallons'] = gals

	# new_list = gals

	# #DB.write_back(list_)

	# DB.update(list_, GALLONS.state == state)

	state_final = data_in['state_final']
	gallons = data_in['gallons_total']


	for item in miles:

		#print(item)

		if state_final.upper() == item['state']:

			print("found here %s" % state_final.upper())

			new_items = item

			new_items['gallons'] = gallons

			DB.update(new_items, MILE_GALLONS.state == state_final.upper() )

		else:
			pass
			#print (" %s not found %s " % (state_final.upper(), item['state']))




def pair_miles_gallons():

	
	#miles = DB_MILES.all()

	gallons = DB_GALLONS.all()

	print(gallons)


	for item in gallons:

		update_miles_gallon(item)







#read miles and state info 

#formart to ABBreviations


#load gallons db

#new data json


#search miles data and insert gallons values in 

#output new miles gallons data

