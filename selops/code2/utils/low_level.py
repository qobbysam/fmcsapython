
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from selenium.webdriver import ActionChains



import sys
sys.path.append('../')

class LowLevelWriter:

	def __init__(self, browser):
		
		self.browser = browser



	def text_input_writer(self, seletorPath, value):

		#INPUT = (By.XPATH, seletorPath)
		input_ = self.browser.find_element_by_xpath(seletorPath)
		input_.send_keys(value )

		print("this text  %s  was entered in this ID %s  " % (value, seletorPath))



	def checkbox_checker(self, seletorPath):

		CHECKBOX_PATH =(By.XPATH, seletorPath)

		checkbox_check = self.browser.find_element_by_xpath(seletorPath).click()

		print("checkbox is clicked here %s " % (checkbox_check) )



	def button_clicker(self, seletorPath):

		#BUTTON_PATH = (By.XPATH, seletorPath)

		button_to_click = self.browser.find_element_by_xpath(seletorPath).click()


		print("button clicked here  %s " % (button_to_click))


	def dropdown_selector(self, selectorpath, value):
		
		select = Select(self.browser.find_element_by_xpath(selectorpath))

# select by visible text
		select.select_by_visible_text(value)

		print("selected %s from dropdown" % value)





	def action_doer(self, actiontype,*pathValueList):

		if actiontype == "enter_text":

			selectorpath, value = pathValueList

			self.text_input_writer(selectorpath, value)

		elif actiontype == "click_btn":

			selectorpath, value = pathValueList

			self.button_clicker(selectorpath)

		elif actiontype == "checkbox_click":

			selectorpath, value = pathValueList

			self.checkbox_checker(selectorpath)



	def miles_gallons_ifta_doer(self,path_data_list, value_data_list, add_btn_path):

		jurisdiction_path, total_miles_path, total_gallons_path = path_data_list

		jurisdiction_value, total_miles_value , total_gallons_value = value_data_list


		self.dropdown_selector(jurisdiction_path, jurisdiction_value)

		self.text_input_writer(total_miles_path, total_miles_value)

		self.text_input_writer(total_gallons_path, total_gallons_value)

		self.button_clicker(add_btn_path)



	def get_value_at_path(self, selectorpath):

		return self.browser.find_element_by_xpath(selectorpath).get_attribute("value")

	
	def click_btn_js(self, selectorpath):

		btn_element = self.browser.find_element_by_xpath(selectorpath)

		#execute_string = "document.getElementByXpath('"+ selectorpath +"').click()"
		#execute_string = "document.evaluate(" + selectorpath + ", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();"
		#self.browser.execute_script("document.getElementByXpath(' ').click()")
		#self.browser.execute_script(execute_string)

		# action = ActionChains(self.browser).move_to_element(btn_element).click(btn_element)
		# action.perform()

		self.browser.execute_script("arguments[0].click();", btn_element)
		#ActionChains(self.browser).click(element).perform()
		

# document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue




# button = ".//button[@class='_42ft _4jy0 layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy']")
# ActionChains(driver).move_to_element(button).perform()
# button.click()
